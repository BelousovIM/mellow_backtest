from setuptools import setup, find_packages


def main():
    with open("requirements.txt") as file:
        requirements = file.readlines()

    setup(
        name="mellow_backtest",
        package_dir={"": "src"},
        packages=find_packages("src"),
        description="backtest strategies",
        install_requires=requirements
    )


if __name__ == "__main__":
    main()
