import numpy as np

from mock import MockGenerator
from strategy import CustomPortfolio
from backtest import Backtest


def test_strategy():
    market_x_cnt = 500
    market_y_cnt = 700
    portfolio_x_cnt = 20
    portfolio_y_cnt = 30
    market = MockGenerator(x_cnt=market_x_cnt, y_cnt=market_y_cnt, timestamp_num=10)
    portfolio = CustomPortfolio(x_cnt=portfolio_x_cnt, y_cnt=portfolio_y_cnt)
    backtest = Backtest(portfolio, market)
    backtest.execute_strategy()
    _, quantities = portfolio._get_quantities()
    first_tokens = np.array(quantities["first_tokens"])
    second_tokens = np.array(quantities["second_tokens"])
    prices = np.array(quantities["prices"])
    assert np.isclose(first_tokens * prices, second_tokens).all(), \
        "the value of USDC part of portfolio does not equal to ETH part of portfolio for all time"
