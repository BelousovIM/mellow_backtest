import numpy as np

from portfolio import BasePortfolio


class MockGenerator:
    """
    Class to generate mocks with price data, where pool fee equals zero
    """

    def __init__(self, x_cnt: float, y_cnt: float, timestamp_num: int = 100):
        self._x_cnt = x_cnt
        self._y_cnt = y_cnt
        self._timestamps = np.linspace(start=0, stop=365, num=timestamp_num)

    def __iter__(self):
        for timestamp in self._timestamps:
            price = self._get_price(noise=True)
            yield timestamp, price

    def _get_price(self, noise: bool) -> float:
        self._add_noise() if noise else None
        return self._x_cnt / self._y_cnt

    def _add_noise(self):
        self._x_cnt *= (1 + np.random.uniform(-20, 20) / 100)
        self._y_cnt *= (1 + np.random.uniform(-20, 20) / 100)
        if self._x_cnt <= 0:
            self._x_cnt = 100
        if self._y_cnt <= 0:
            self._y_cnt = 100

    def swap_tokens(self, *, portfolio: BasePortfolio, tokens_cnt: float, forward: bool):
        """
        Allow to swap token X for token Y or vice versa

        :param portfolio: Class for portfolio simulation
        :param tokens_cnt: Count of tokens to swap
        :param forward: if True swap token X for token Y else vice versa
        """
        price = self._get_price(noise=False)
        if forward:
            portfolio.change_balance(x_cnt=-tokens_cnt, y_cnt=tokens_cnt * price)
        else:
            portfolio.change_balance(x_cnt=tokens_cnt / price, y_cnt=-tokens_cnt)
