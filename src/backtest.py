from portfolio import BasePortfolio
from mock import MockGenerator


class Backtest:
    """
    Class to backtest your strategies
    """

    def __init__(self, portfolio: BasePortfolio, market: MockGenerator):
        self._portfolio = portfolio
        self._market = market

    def show_history(self):
        self._portfolio.show_history()

    def execute_strategy(self, show_history: bool = False):
        self._portfolio._clear_history()
        for timestamp, price in self._market:
            deal = self._portfolio[price]
            self._portfolio._log(timestamp=timestamp, price=price)
            self._market.swap_tokens(
                portfolio=self._portfolio,
                tokens_cnt=deal.tokens_cnt,
                forward=deal.forward
            )
        if show_history:
            self.show_history()
