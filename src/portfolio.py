from typing import Optional, Tuple, List, Dict
from dataclasses import dataclass

from matplotlib import pyplot as plt
import seaborn as sns
sns.set_theme()


@dataclass
class Deal:
    forward: bool
    tokens_cnt: float


class BasePortfolio:
    """
    Class for portfolio simulation
    """

    def __init__(self, x_cnt: float, y_cnt: float, first_token: str, second_token: str):
        self._first_token_name = first_token
        self._second_token_name = second_token
        self._x_cnt = x_cnt
        self._y_cnt = y_cnt
        self._operations_history = []

    def _strategy(self, price: float) -> Deal:
        raise NotImplementedError

    def __getitem__(self, price: float) -> Deal:
        return self._strategy(price=price)

    def get_tokens_cnt(self) -> Tuple[float, float]:
        return self._x_cnt, self._y_cnt

    def get_tokens_name(self) -> Tuple[str, str]:
        return self._first_token_name, self._second_token_name

    def change_balance(self, x_cnt: Optional[float] = None, y_cnt: Optional[float] = None):
        if x_cnt:
            self._x_cnt += x_cnt
        if y_cnt:
            self._y_cnt += y_cnt

    def _log(self, timestamp: float, price: float):
        x_cnt, y_cnt = self.get_tokens_cnt()
        first_token_total = y_cnt / price + x_cnt
        second_token_total = y_cnt + x_cnt * price
        history_item = dict(
            timestamp=timestamp,
            first_token_total=first_token_total,
            second_token_total=second_token_total,
            price=price
        )
        self._operations_history.append(history_item)

    def _clear_history(self):
        self._operations_history.clear()

    @staticmethod
    def _plot(x: List[float], y: List[float], ax: plt.Axes, label: str, xlabel: str = "timestamp"):
        ax.plot(x, y, label=label)
        ax.set_xlabel(xlabel=xlabel)
        plt.subplots_adjust(hspace=0.4)
        ax.legend()

    def _get_quantity(self, quantity_name: str) -> List[float]:
        return [history_item[quantity_name] for history_item in self._operations_history]

    def _get_quantities(self) -> Tuple[List[float], Dict[str, List[float]]]:
        timestamps = self._get_quantity(quantity_name="timestamp")
        quantities = dict(
            first_tokens=self._get_quantity(quantity_name="first_token_total"),
            second_tokens=self._get_quantity(quantity_name="second_token_total"),
            prices=self._get_quantity(quantity_name="price")
        )
        return timestamps, quantities

    def show_history(self):
        timestamps, quantities = self._get_quantities()
        fig, axs = plt.subplots(nrows=3, ncols=1, figsize=(15, 10))
        labels = [*self.get_tokens_name(), "Price"]
        for ax_i, (label, quantity) in enumerate(zip(labels, quantities.values())):
            self._plot(x=timestamps, y=quantity, ax=axs[ax_i], label=label)
