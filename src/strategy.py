from portfolio import BasePortfolio, Deal


class CustomPortfolio(BasePortfolio):

    def __init__(self, x_cnt: float, y_cnt: float,  first_token: str = "USDC", second_token: str = "ETH"):
        super().__init__(x_cnt=x_cnt, y_cnt=y_cnt, first_token=first_token, second_token=second_token)

    def _strategy(self, price: float) -> Deal:
        x_cnt, y_cnt = self.get_tokens_cnt()
        if y_cnt > x_cnt * price:
            tokens_cnt = (y_cnt - x_cnt * price) / 2
            return Deal(forward=False, tokens_cnt=tokens_cnt)
        else:
            tokens_cnt = (x_cnt * price - y_cnt) / (2 * price)
            return Deal(forward=True, tokens_cnt=tokens_cnt)
