# MELLOW_BACKTEST
Python framework for backtesting trading strategy

### Installation
```
git clone https://gitlab.com/BelousovIM/mellow_backtest.git
pip install -e mellow_backtest
```

### CI / DI
If you want test example strategy go to __CI/CD__ -> __Pipelines__ -> __Run pipeline__
and execute __test__ . Example strategy is to keep the value of USDC part of portfolio 
equal to ETH part of portfolio.

### Description of classes
   * __MockGenerator__ - class to generate mocks with price data
   * __BasePortfolio__ - base class for portfolio simulation and also parent for classes with strategies.
     
     __CustomPortfolio__ - child class with example strategy
   * __Backtest__ - class to backtest your strategies

### Usage
Go to folder __./notebooks__ for examples